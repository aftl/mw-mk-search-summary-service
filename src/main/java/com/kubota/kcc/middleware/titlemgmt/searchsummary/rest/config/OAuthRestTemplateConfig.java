package com.kubota.kcc.middleware.titlemgmt.searchsummary.rest.config;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;

import com.kubota.kcc.middleware.titlemgmt.searchsummary.request.metrics.ApplicationMetrics;

@ConditionalOnProperty(value = "oauth.enable", havingValue = "true", matchIfMissing = true)
@Configuration
public class OAuthRestTemplateConfig {
	@Value("${wkApiAccessTokenUri}")
    private String accessTokenUri;

    @Value("${wkApiClientId}")
    private String clientId;

    @Value("${wkApiClientSecret}")
    private String clientSecret;
    
    @Value("${wkApiGrantType}")
    private String grantType;
    
    @Value("${wkApiSearchSummaryConnectTimeout}")
    private Integer apiConnectTimeout;
    
    @Value("${wkApiSearchSummaryReadTimeout}")
    private Integer apiReadTimeout;
    
    @Bean
    public OAuth2ProtectedResourceDetails oAuth2ResourceDetails() {
        ClientCredentialsResourceDetails details = new ClientCredentialsResourceDetails();
        details.setId("woltersklewer");
        details.setClientId(clientId);
        details.setClientSecret(clientSecret);
        details.setAccessTokenUri(accessTokenUri);
        details.setTokenName("Authorization");        
        details.setGrantType(grantType);
        return details;
    }
    
	@Bean
	public OAuth2RestTemplate oauth2RestTemplate(Logger applogger, ApplicationMetrics applicationMetrics, OAuth2ProtectedResourceDetails details) {
		OAuth2RestTemplate oAuth2RestTemplate = new OAuth2RestTemplate(details);
		
		/* To validate if required configurations are in place during startup */
		try {
			long startTime = System.nanoTime();
			oAuth2RestTemplate.getAccessToken();
			applicationMetrics.logOAuthTokenResponseTime(startTime);
			
			applogger.debug("Retrieved OAuth2 Access Token");
		} catch (Exception ex) {
			applogger.fatal("Error Retrieving OAuth Token from URI: [{}]", accessTokenUri, ex);
		}
						
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setConnectTimeout(apiConnectTimeout);
		factory.setReadTimeout(apiReadTimeout);
		
		BufferingClientHttpRequestFactory bufferingRequestFactory = new BufferingClientHttpRequestFactory(factory);
		oAuth2RestTemplate.setRequestFactory(bufferingRequestFactory);
		
		List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
    	interceptors.add(new RequestResponseLoggingInterceptor());    	
    	oAuth2RestTemplate.setInterceptors(interceptors);
		
		return oAuth2RestTemplate;
	}

}
