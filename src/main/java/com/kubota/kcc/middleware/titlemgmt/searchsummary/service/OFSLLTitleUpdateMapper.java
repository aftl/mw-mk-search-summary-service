package com.kubota.kcc.middleware.titlemgmt.searchsummary.service;

import org.springframework.stereotype.Component;

import com.kubota.kcc.middleware.titlemgmt.searchsummary.domain.OFSLLTitleUpdate;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.domain.SearchRequest;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.model.Confirmation;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.model.SearchSummaryResponse;

@Component
public class OFSLLTitleUpdateMapper {
	/**
	 * Method to map OFSLL asset and SearchSummaryResponse to OFSLLTitleUpdate for use to output to file
	 * 
	 * @param search
	 * @param SearchSummaryResponse
	 * @return OFSLLTitleUpdate
	 */
	OFSLLTitleUpdate mapSuccessfulTransaction(SearchRequest search, SearchSummaryResponse response) {
		OFSLLTitleUpdate titleUpdate = new OFSLLTitleUpdate();
		
		titleUpdate.setAssetNumber(search.getAssetNumber());
		titleUpdate.setLoanAccountNbr(search.getAccountNumber());
		titleUpdate.setSuccessTxn(true);
		titleUpdate.setTitleStatus(response.getVehicles().get(0).getTitleStatus());
		titleUpdate.setTitleType(response.getVehicles().get(0).getType());
		titleUpdate.setVin(response.getVehicles().get(0).getVin());
		titleUpdate.setVlpref(response.getVehicles().get(0).getVlpref().toString());
				
		return titleUpdate;
	}
	
	/**
	 * Method to map unsuccessful WK transaction
	 * @param SearchRequest
	 * @return OFSLLTitleUpdate
	 */
	OFSLLTitleUpdate mapUnsuccessfulTransaction(SearchRequest search, SearchSummaryResponse response) {
		OFSLLTitleUpdate titleUpdate = new OFSLLTitleUpdate();
			
		titleUpdate.setAssetNumber(search.getAssetNumber());		
		titleUpdate.setLoanAccountNbr(search.getAccountNumber());		
		titleUpdate.setSuccessTxn(false);
		titleUpdate.setVin(search.getVin());
		titleUpdate.setVlpref(search.getVlref());
		
		if( response != null ) {
			Confirmation conf = response.getConfirmation();
			
			if( conf != null ) {
				titleUpdate.setComment(conf.getMessage());
				titleUpdate.setTitleStatus("Error");
			} else {				
				titleUpdate.setComment("No API Confirmation Message");
				titleUpdate.setTitleStatus("Error");
			}
		} else {
			titleUpdate.setComment("Middleware Service Exception. Contact KCC App Support.");
			titleUpdate.setTitleStatus("Error");
			titleUpdate.setVlpref("");
		}
		
		return titleUpdate;
	}
}
