package com.kubota.kcc.middleware.titlemgmt.searchsummary.service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.domain.OFSLLTitleUpdate;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.domain.SearchRequest;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.exception.SearchSummaryException;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.model.SearchSummaryResponse;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.request.metrics.ApplicationMetrics;
import com.opencsv.ICSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;


@Service
public class SearchSummaryService {
	@Autowired
	Logger applogger;
	
	@Autowired
	ApplicationMetrics applicationMetrics;
	
	@Autowired
	OAuth2RestTemplate oAuth2RestTemplate;
		
	@Autowired
	WKSearchSummaryRequestMapper wkMapper;
	
	@Autowired
	OFSLLTitleUpdateMapper ofsllMapper;
	
	@Value("${ofsll.createrecord.sharedFolder}")
	String ofsllOutputDirectory;
	
	@Value("${wkApiSearchSummaryURI}")
	private String searchSummaryURI;
	
	ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	
	static final String OUTPUT_FILENAME_BASE = "Lien_search_";
	static final String OURPUT_FILENAME_EXT = ".dat";
	static final String CONFIRMATION_MESSAGE_NOT_FOUND = "No records found for the specified search criteria";
	
	/**
	 * Method to process the search request records from OFSLL
	 * @param searchRequestList
	 * @return List<OFSLLTitleUpdate>
	 */
	public void searchRecords(List<SearchRequest> searchRequestList) throws SearchSummaryException {
		List<OFSLLTitleUpdate> ofsllTitleUpdateList = new ArrayList<>();
		ThreadContext.put("appName", "mw-wk-search-summary-service");
		
		try {			
			applogger.trace("Incoming Search Summary Requests: [{}]", searchRequestList.size());
			
			// Process assets to WK Create Record API
			ofsllTitleUpdateList = searchRequestList.stream()
										.map(this :: callSearchSummaryService)
										.collect(Collectors.toList());
			
			applogger.trace("Successful Search Summary Txns: [{}]", ofsllTitleUpdateList.stream().filter(OFSLLTitleUpdate :: getSuccessTxn).count());
			applogger.trace("Unsuccessful Search Summary Txns: [{}]", ofsllTitleUpdateList.stream().filter(titleUpdate -> !titleUpdate.getSuccessTxn()).count());
			
			// Write successful results out to delimited file for OFSLL
			writeResultToOutput(ofsllTitleUpdateList);
		} catch (OAuth2Exception|RestClientException ex) {
			throw new SearchSummaryException(ex);	
		} catch (Exception ex) {
			applogger.fatal("Exception: ", ex);
			throw new SearchSummaryException(ex);
		}  
	}
	
	/**
	 * Method that calls the WK Search Summary API with an SearchRequest
	 * @param searchReq 
	 * @return OFSLLTitleUpdate
	 * @throws OAuth2Exception, RestClientException
	 */
	OFSLLTitleUpdate callSearchSummaryService(SearchRequest searchReq) throws OAuth2Exception, RestClientException {
		OFSLLTitleUpdate ofsllTitleUpdate = new OFSLLTitleUpdate();
		
		try {
			applicationMetrics.incrementRequestCount();
			// Create HTTP Headers and request JSON
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> request = new HttpEntity<>(ow.writeValueAsString(wkMapper.mapSearchSummaryRequest(searchReq)), headers);
			
			applogger.trace("Search Request: Account: [{}] VIN: [{}] ", searchReq.getAccountNumber(), searchReq.getVin());
			long startTime = System.nanoTime();
			ResponseEntity<SearchSummaryResponse> response = oAuth2RestTemplate.postForEntity(searchSummaryURI, request, SearchSummaryResponse.class);			
			applicationMetrics.logApiResponseTime(startTime);
			
			// Handle response
			if( Boolean.FALSE.equals(responseHasError(response)) ) {
				applogger.trace("API Success Response: Account: [{}] VIN: [{}]", searchReq.getAccountNumber(), searchReq.getVin());
				ofsllTitleUpdate = ofsllMapper.mapSuccessfulTransaction(searchReq, response.getBody());
			} else {
				applogger.trace("API Error Response: Account: [{}] VIN: [{}]", searchReq.getAccountNumber(), searchReq.getVin());
				ofsllTitleUpdate = ofsllMapper.mapUnsuccessfulTransaction(searchReq, response.getBody());
			}
		} catch (OAuth2Exception ex) {
			applogger.fatal("OAuth2 Exception: {}", ex.getSummary());	
			throw ex;
		} catch (RestClientException ex) {
			applogger.fatal("RestClientException Exception:", ex);
			throw ex;
		} catch (JsonProcessingException ex) {
			applogger.fatal("JsonProcessingException Exception:", ex);
			throw new SearchSummaryException(ex);
		} catch (SearchSummaryException ex) {
			applogger.error("SearchSummaryException Exception: ", ex);
			applogger.fatal("SearchSummaryException Exception: ", ex);
			applogger.trace("Search Request Error: [{}]", ex.getMessage());
			ofsllTitleUpdate = ofsllMapper.mapUnsuccessfulTransaction(searchReq, null);
		} 
		
		return ofsllTitleUpdate;
	}
	
	
	/**
	 * Method to write the result list of OFSLLTitleUpdate objects to delimited file for OFSLL to read
	 * @param updateList
	 * @throws SearchSummaryException
	 */
	void writeResultToOutput(List<OFSLLTitleUpdate> updateList) throws SearchSummaryException {
	    try(Writer writer = new FileWriter(generateCurrentFileName())) {
		    StatefulBeanToCsv<OFSLLTitleUpdate> beanToCsv = 
		    										new StatefulBeanToCsvBuilder<OFSLLTitleUpdate>(writer).withSeparator('|')
																    									  .withIgnoreField(OFSLLTitleUpdate.class, 
																											  	       OFSLLTitleUpdate.class.getField("successTxn"))
																    									  .withQuotechar(ICSVWriter.NO_QUOTE_CHARACTER)
																								  		  .build();
			beanToCsv.write(updateList.stream());
		} catch (CsvDataTypeMismatchException | CsvRequiredFieldEmptyException| NoSuchFieldException ex) {
			applogger.fatal("CSV Exception", ex);	
			throw new SearchSummaryException(ex);
		} catch (IOException ex) {
			applogger.fatal("IO Exception", ex);
			throw new SearchSummaryException(ex);
		} 
	}
		
	/**
	 * Method to generate the path and filename based on the current date
	 * 
	 * @return String
	 * @throws SearchSummaryException
	 */
	String generateCurrentFileName() throws SearchSummaryException {
		LocalDateTime ldt = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMddyyyy_HHmmss", Locale.ENGLISH);		
		
		String filename = ofsllOutputDirectory + OUTPUT_FILENAME_BASE + ldt.format(dtf) + OURPUT_FILENAME_EXT;
		applogger.trace("Output to file: [{}]", filename);
		
		return filename;
	}
	
	/**
	 * Method to check the WK response for indication of error. 
	 * HTTP Response code cannot be used as only error indicator according to WK tech support.
	 * 
	 * @param response
	 * @return Boolean
	 * @throws CreateRecordException
	 */
	Boolean responseHasError(ResponseEntity<SearchSummaryResponse> response) throws SearchSummaryException {
		Boolean hasError = false;
		if( response != null ) {
			SearchSummaryResponse body = response.getBody();
			
			if( !response.getStatusCode().is2xxSuccessful() ) {
				applogger.fatal("API Error Response Code: [{}] Reponse Body: [{}]", response.getStatusCode(), response.getBody());
				applogger.trace("API Error Response Code: [{}] Reponse Body: [{}]", response.getStatusCode(), response.getBody());
				hasError = true;
			} else if( body != null ) {
				if( Boolean.FALSE.equals(body.getValid()) ) {
					applogger.fatal("API Error Valid Flag False. Reponse Body: [{}]", response.getBody());
					applogger.trace("API Error Valid Flag False. Reponse Body: [{}]", response.getBody());
					hasError = true;
				} else if( body.getConfirmation() != null && CONFIRMATION_MESSAGE_NOT_FOUND.equals(body.getConfirmation().getMessage()) ) {
					applogger.fatal("API Error In Confirmation Message. No records found. Reponse Body: [{}]", response.getBody());
					applogger.trace("API Error In Confirmation Message. No records found. Reponse Body: [{}]", response.getBody());
					hasError = true;
				}
			} else {
				applogger.fatal("API Error. Response Body Null.");
				applogger.trace("API Error. Response Body Null.");
				hasError = true;
			}
		} else
			hasError = true;
		
		return hasError;
	}
}
