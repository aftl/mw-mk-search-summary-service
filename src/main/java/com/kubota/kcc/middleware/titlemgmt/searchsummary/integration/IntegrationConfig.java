package com.kubota.kcc.middleware.titlemgmt.searchsummary.integration;

import java.util.ArrayList;

import javax.persistence.EntityManagerFactory;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.jpa.core.JpaExecutor;
import org.springframework.integration.jpa.inbound.JpaPollingChannelAdapter;
import org.springframework.integration.scheduling.PollerMetadata;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

import com.kubota.kcc.middleware.titlemgmt.searchsummary.domain.SearchRequest;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.service.SearchSummaryService;

@ConditionalOnProperty(value = "spring.integration.enable", havingValue = "true", matchIfMissing = true)
@Configuration
@EnableIntegration
public class IntegrationConfig {

	@Autowired
	Logger applogger;

	@Value("${poller.cron}")
	private String pollerCronExpr;

	@Bean
	public JpaExecutor jpaExecutor(EntityManagerFactory entityManagerFactory) {
		JpaExecutor executor = new JpaExecutor(entityManagerFactory);
		executor.setJpaQuery("from SearchRequest");
		return executor;
	}

	@Bean
	MessageChannel jpaInputChannel() {
		return new DirectChannel();
	}

	@Bean
	PublishSubscribeChannel intErrorChannel() {
		return new PublishSubscribeChannel();
	}

	@Bean
	public PollerMetadata pollerMetadata(SearchSummaryPollerAdvice createRecordPollerAdvice) {
		return Pollers.cron(pollerCronExpr)
					  .advice(createRecordPollerAdvice)
					  .errorChannel(intErrorChannel())
					  .maxMessagesPerPoll(1)
					  .get();
	}

	@Bean
	@InboundChannelAdapter(channel = "jpaInputChannel", poller = @Poller(value = "pollerMetadata"))
	public JpaPollingChannelAdapter jpaInbound(JpaExecutor jpaExecutor) {
		return new JpaPollingChannelAdapter(jpaExecutor);
	}

	@Bean
	@ServiceActivator(inputChannel = "jpaInputChannel")
	public MessageHandler handler(SearchSummaryService searchSummaryService) {
		return message -> {
			ArrayList<SearchRequest> list = (ArrayList<SearchRequest>) message.getPayload();
			try {
				searchSummaryService.searchRecords(list);
			} catch (Exception e) {
				throw new MessagingException(message, e.getCause());
			}
		};
	}

	@Bean
	@ServiceActivator(inputChannel = "intErrorChannel", outputChannel = "nullChannel")
	public MessageHandler handleError(ErrorHandler errorHandler) {
		return (errorHandler::logError);
	}
}
