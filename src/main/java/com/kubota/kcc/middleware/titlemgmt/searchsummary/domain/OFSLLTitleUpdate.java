package com.kubota.kcc.middleware.titlemgmt.searchsummary.domain;

import java.io.Serializable;

import com.opencsv.bean.CsvBindByPosition;

public class OFSLLTitleUpdate implements Serializable {	

	private static final long serialVersionUID = -3009249057239518789L;
	
	@CsvBindByPosition(position = 0)
	String vlpref;
	
	@CsvBindByPosition(position = 1)
	String vin;
	
	@CsvBindByPosition(position = 2)
	String loanAccountNbr;
	
	@CsvBindByPosition(position = 3)
	String assetNumber;
	
	@CsvBindByPosition(position = 4)
	String titleStatus;
	
	@CsvBindByPosition(position = 5)
	String comment;
	
	@CsvBindByPosition(position = 6)
	String titleType;
	
	public Boolean successTxn;

	public String getVlpref() {
		return vlpref;
	}

	public void setVlpref(String vlpref) {
		this.vlpref = vlpref;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getLoanAccountNbr() {
		return loanAccountNbr;
	}

	public void setLoanAccountNbr(String loanAccountNbr) {
		this.loanAccountNbr = loanAccountNbr;
	}

	public String getAssetNumber() {
		return assetNumber;
	}

	public void setAssetNumber(String assetNumber) {
		this.assetNumber = assetNumber;
	}

	public String getTitleStatus() {
		return titleStatus;
	}

	public void setTitleStatus(String titleStatus) {
		this.titleStatus = titleStatus;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Boolean getSuccessTxn() {
		return successTxn;
	}

	public void setSuccessTxn(Boolean successTxn) {
		this.successTxn = successTxn;
	}

	public String getTitleType() {
		return titleType;
	}

	public void setTitleType(String titleType) {
		this.titleType = titleType;
	}	
}
