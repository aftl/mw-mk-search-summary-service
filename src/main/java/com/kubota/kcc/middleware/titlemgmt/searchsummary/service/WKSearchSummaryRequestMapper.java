package com.kubota.kcc.middleware.titlemgmt.searchsummary.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.kubota.kcc.middleware.titlemgmt.searchsummary.domain.SearchRequest;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.exception.SearchSummaryException;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.model.OrderHeader;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.model.OrderItem;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.model.OrderItemHeader;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.model.SearchCriteria;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.model.SearchSummaryRequest;

@Component
public class WKSearchSummaryRequestMapper {
	public static final String TITLE_MGMT_ORDER_TYPE = "ILIENMV.TITLEMANAGEMENT";
	public static final String TITLE_MGMT_ITEM_TYPE_SEARCH_SUMMARY = "SearchSummary";
	
	SearchSummaryRequest mapSearchSummaryRequest(SearchRequest input) throws SearchSummaryException {
		SearchSummaryRequest request = new SearchSummaryRequest();
		
		request.setOrderHeader(mapOrderHeader());
		request.setOrderItems(mapOrderItems());
		request.setSearchCriteria(mapSearchCriteria(input));
		
		return request;
	}
	
	OrderHeader mapOrderHeader() {
		OrderHeader orderHeader = new OrderHeader();		
		orderHeader.setOrderType(TITLE_MGMT_ORDER_TYPE);

		return orderHeader;
	}
	
	List<OrderItem> mapOrderItems() {
		List<OrderItem> orderItemList = new ArrayList<>();
		
		OrderItem orderItem = new OrderItem();
		OrderItemHeader orderItemHeader = new OrderItemHeader();
		orderItemHeader.setItemType(TITLE_MGMT_ITEM_TYPE_SEARCH_SUMMARY);
		orderItem.setOrderItemHeader(orderItemHeader);
		orderItemList.add(orderItem);
		
		return orderItemList;
	}
	
	SearchCriteria mapSearchCriteria(SearchRequest input) throws SearchSummaryException {
		SearchCriteria data = new SearchCriteria();
		
		if( input.getVlref() == null || input.getVlref().equals(""))
			throw new SearchSummaryException("Missing required VLREF value. Account: [" + input.getAccountNumber()+ "] VIN: [" + input.getVin() + "]");
		else
			data.setSearchKey(input.getVlref());
		
		return data;
	}
}
