package com.kubota.kcc.middleware.titlemgmt.searchsummary.rest.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

/*
 * This interceptor class is called by the RestTemplate class. 
 * See RestTemplateGenerator class for use.
 * It will allow logging of the request/response to/from OFSLL.
 */
public class RequestResponseLoggingInterceptor implements ClientHttpRequestInterceptor {
	Logger applogger = LogManager.getLogger("applogger");
		
	@Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
		ThreadContext.put("appName", "mw-wk-search-summary-service");
        traceRequest(request, body);
        ClientHttpResponse response = execution.execute(request, body);
        traceResponse(response);
        return response;
    }

	private void traceRequest(HttpRequest request, byte[] body) {
    	applogger.info("===========================request begin================================================");
    	applogger.debug("URI         : {}", request.getURI());
    	applogger.debug("Method      : {}", request.getMethod());
    	applogger.debug("Headers     : {}", request.getHeaders() );
    	if( applogger.isDebugEnabled() )
    		applogger.debug("Request body: {}", new String(body, StandardCharsets.UTF_8));
    	applogger.info("==========================request end================================================");
    }

    private void traceResponse(ClientHttpResponse response) throws IOException {
        StringBuilder inputStringBuilder = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getBody(), StandardCharsets.UTF_8));
        String line = bufferedReader.readLine();
        while (line != null) {
            inputStringBuilder.append(line);
            inputStringBuilder.append('\n');
            line = bufferedReader.readLine();
        }
        applogger.info("============================response begin==========================================");
        applogger.debug("Status code  : {}", response.getStatusCode());
        applogger.debug("Status text  : {}", response.getStatusText());
        applogger.debug("Headers      : {}", response.getHeaders());
        if( applogger.isDebugEnabled() )
        	applogger.debug("Response body: {}", inputStringBuilder);
        applogger.info("=======================response end=================================================");
    }

}
