package com.kubota.kcc.middleware.titlemgmt.searchsummary.integration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;
import org.apache.logging.log4j.Logger;

@Component
public class ErrorHandler {
	
	@Autowired
	Logger applogger;

	public void logError(Message<?> message) {
		MessagingException exception = (MessagingException) message.getPayload();
		applogger.fatal("Error Processing OFSLL Search Summary Data: {}", exception.getMessage());
	}
}