package com.kubota.kcc.middleware.titlemgmt.searchsummary.exception;

public class SearchSummaryException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4021263160578023795L;
	
	public SearchSummaryException(Exception ex) {
		super(ex);
	}
	
	public SearchSummaryException(String msg) {
		super(msg);
	}

}
