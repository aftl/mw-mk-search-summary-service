package com.kubota.kcc.middleware.titlemgmt.searchsummary;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import io.micrometer.core.instrument.MeterRegistry;

@SpringBootApplication
public class MwWkSearchSummaryApplication {
	@Bean
	public Logger applogger() {
		return LogManager.getLogger("applogger");
	}
	
	@Bean
    MeterRegistryCustomizer<MeterRegistry> metricsCommonTags() {
		return registry -> registry.config().commonTags("application", "mw-wk-search-summary-service");
    }
	
	public static void main(String[] args) {
		ThreadContext.put("appName", "mw-wk-search-summary-service");
		SpringApplication.run(MwWkSearchSummaryApplication.class, args);
	}

}
