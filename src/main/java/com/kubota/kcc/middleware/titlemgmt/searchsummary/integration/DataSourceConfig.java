package com.kubota.kcc.middleware.titlemgmt.searchsummary.integration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceConfig {
	@Value("${datasource.driver-class}")
	String driver;
	
	@Value("${datasource.url}")
	String url;
	
	@Value("${datasource.username}")
	String datasourceUser;
	
	@Value("${datasource.password}")
	String datasourcePassword;
	
	@Value("${oracle.db.username}")
	String vaultUser;
	
	@Value("${oracle.db.password}")
	String vaultPassword;
	
	@Bean
    public DataSource getDataSource() {
        DataSourceBuilder<?> dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(driver);
        dataSourceBuilder.url(url);
        dataSourceBuilder.username(datasourceUser.equals("") ? vaultUser : datasourceUser);
        dataSourceBuilder.password(datasourcePassword.equals("") ? vaultPassword : datasourcePassword);
        return dataSourceBuilder.build();
    }
}
