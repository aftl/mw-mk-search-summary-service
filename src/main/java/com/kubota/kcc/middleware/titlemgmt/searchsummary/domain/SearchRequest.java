package com.kubota.kcc.middleware.titlemgmt.searchsummary.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "LIEN_PENDING_SEARCH_KCC")
public class SearchRequest {
	@Id
	@Column(name = "ASE_NBR")
	private String assetNumber;
	
	@NotNull
	@Column(name = "ACC_NBR")
	private String accountNumber;	
	
	@Column(name = "ASE_IDENTIFICATION_NBR")
	private String vin;
	
	@Column(name = "VLREF")
	private String vlref;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAssetNumber() {
		return assetNumber;
	}

	public void setAssetNumber(String assetNumber) {
		this.assetNumber = assetNumber;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getVlref() {
		return vlref;
	}

	public void setVlref(String vlref) {
		this.vlref = vlref;
	}
}
