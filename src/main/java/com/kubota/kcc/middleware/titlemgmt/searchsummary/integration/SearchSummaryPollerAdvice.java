package com.kubota.kcc.middleware.titlemgmt.searchsummary.integration;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.aop.MessageSourceMutator;
import org.springframework.integration.core.MessageSource;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.kubota.kcc.middleware.titlemgmt.searchsummary.domain.SearchRequest;

@Component
public class SearchSummaryPollerAdvice implements MessageSourceMutator {

	@Autowired
	Logger applogger;
	
	@Override
	public boolean beforeReceive(MessageSource<?> source) {
		ThreadContext.put("appName", "mw-wk-search-summary-service");
		return true;
	}
	
	@Override
	public Message<?> afterReceive(Message<?> result, MessageSource<?> source) {		
		if( result == null )
			applogger.fatal("No OFSLL Search Summary Data Available");
		else {
			List<SearchRequest> resultList = (List<SearchRequest>) result.getPayload();
			applogger.trace("Search Summary Result Size: [{}]", resultList.size());
		}
		return result;
	}

}
