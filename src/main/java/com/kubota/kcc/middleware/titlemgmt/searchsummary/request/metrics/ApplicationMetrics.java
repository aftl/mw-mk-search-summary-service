package com.kubota.kcc.middleware.titlemgmt.searchsummary.request.metrics;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApplicationMetrics {
	@Autowired
	Logger appLogger;
	
    @Autowired
    MeterRegistry registry;

    public void incrementRequestCount() {
        Counter recordCounter = registry.counter("ofsll.titlemgmt.searchsummary.reqs");
        recordCounter.increment();
    }
    
    public void logApiResponseTime(long startTime) {
    	ThreadContext.put("appName", "mw-wk-search-summary-service");
        Timer timer = registry.timer("ofsll.titlemgmt.searchsummary.api.response.time");
        long elapsedTime = System.nanoTime() - startTime;
        long elapsedTimeInSecond = elapsedTime / 1_000_000_000;
        appLogger.trace("Search Summary API Response Time: [{}]", elapsedTimeInSecond);
        timer.record(elapsedTimeInSecond, TimeUnit.SECONDS);
    } 
    
    public void logOAuthTokenResponseTime(long startTime) {
    	ThreadContext.put("appName", "mw-wk-search-summary-service");
        Timer timer = registry.timer("ofsll.titlemgmt.searchsummary.oauth.response.time");
        long elapsedTime = System.nanoTime() - startTime;
        long elapsedTimeInSecond = elapsedTime / 1_000_000_000;
        appLogger.trace("Search Summary OAuth Token Response Time: [{}]", elapsedTimeInSecond);
        timer.record(elapsedTimeInSecond, TimeUnit.SECONDS);
    }
}
