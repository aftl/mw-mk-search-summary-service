package com.kubota.kcc.middleware.titlemgmt.searchsummary.service;

import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestClientException;

import com.kubota.kcc.middleware.titlemgmt.searchsummary.domain.OFSLLTitleUpdate;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.domain.SearchRequest;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.exception.SearchSummaryException;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.model.Confirmation;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.model.SearchSummaryResponse;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.model.Vehicle;


@SpringBootTest
@ActiveProfiles("test")
class SearchSummaryServiceTest {
	@MockBean
	OAuth2RestTemplate oAuth2RestTemplate;
		
	@Autowired
	SearchSummaryService service;
	
	ResponseEntity<SearchSummaryResponse> successResponse;
	ResponseEntity<SearchSummaryResponse> unsuccessfulResponse;
	List<SearchRequest> testSearchList;
	List<OFSLLTitleUpdate> testOfsllTitleUpdateList;
	
	@Value("${ofsll.createrecord.sharedFolder}")
	String ofsllOutputDirectory;
	
	@BeforeEach
	public void setup() {
		successResponse = buildMockSuccessResponse();
		unsuccessfulResponse = buildMockUnsuccessfulResponse();
		testSearchList = buildTestSearchList();
		testOfsllTitleUpdateList = buildTestOfsllTitleUpdateList();
		
		when(oAuth2RestTemplate.getOAuth2ClientContext()).thenReturn(new DefaultOAuth2ClientContext(new DefaultAccessTokenRequest()));
	    when(oAuth2RestTemplate.getAccessToken()).thenReturn(new DefaultOAuth2AccessToken("my-fake-token"));
	}
	
	public ResponseEntity<SearchSummaryResponse> buildMockSuccessResponse() {
		ResponseEntity<SearchSummaryResponse> test = new ResponseEntity<>(new SearchSummaryResponse(), HttpStatus.CREATED);
				
		
		test.getBody().setItemType("SearchSummary");
		test.getBody().setOrderReferenceId("785ff21e-eedb-4ce6-a6eb-d08011c04471");
		test.getBody().setValid(true);
		test.getBody().setVehicleCount(new BigInteger("1"));
		
		Vehicle vehicle = new Vehicle();
		vehicle.setBillCode("Ref-Test-1");
		vehicle.setBorrowerName("JOHN DOE");
		vehicle.setTitleState("TX");
		vehicle.setTitleStatus("Pending Title Notification");
		vehicle.setType("Title Storage");
		vehicle.setVin("VINTEST");
		vehicle.setVlpref(new BigInteger("339351"));
		
		List<Vehicle> vehicleList = new ArrayList<>();
		vehicleList.add(vehicle);
		
		test.getBody().setVehicles(vehicleList);
		
		return test;
	}
	
	public ResponseEntity<SearchSummaryResponse> buildMockUnsuccessfulResponse() {
		ResponseEntity<SearchSummaryResponse> test = new ResponseEntity<>(new SearchSummaryResponse(), HttpStatus.OK);
		
		Confirmation conf = new Confirmation();
		conf.setMessage("No records found for the specified search criteria");
		test.getBody().setConfirmation(conf);
		test.getBody().setItemType("Search Summary");
		test.getBody().setOrderReferenceId("785ff21e-eedb-4ce6-a6eb-d08011c04471");
		test.getBody().setValid(true);
		
		return test;
	}
	
	public ArrayList<SearchRequest> buildTestSearchList() {
		ArrayList<SearchRequest> searchList = new ArrayList<>();
		
		SearchRequest asset = new SearchRequest();		
		asset.setAccountNumber("1234578");
		asset.setAssetNumber("91921");
		asset.setVin("VINTEST");
		asset.setVlref("339351");
		
		searchList.add(asset);
		
		return searchList;
	}
	
	public List<OFSLLTitleUpdate> buildTestOfsllTitleUpdateList() {
		List<OFSLLTitleUpdate> list = new ArrayList<>();
		
		OFSLLTitleUpdate update = new OFSLLTitleUpdate();
		update.setAssetNumber("111");
		update.setLoanAccountNbr("12345678");
		update.setSuccessTxn(true);
		update.setTitleStatus("Pending Title Notification");
		update.setVin("98989");
		update.setVlpref("112233");
		list.add(update);
		
		return list;
	}
	
	@Test
	void testCallSearchSummaryService_Success() throws Exception {
		when(oAuth2RestTemplate.postForEntity(Mockito.anyString(), 
											  Mockito.any(Object.class), 
											  Mockito.eq(SearchSummaryResponse.class)))
		.thenReturn(successResponse);
			
		OFSLLTitleUpdate ofsllTitleUpdate = service.callSearchSummaryService(testSearchList.get(0));
		
		assertEquals("91921", ofsllTitleUpdate.getAssetNumber());
		assertEquals("1234578", ofsllTitleUpdate.getLoanAccountNbr());
		assertEquals("Pending Title Notification", ofsllTitleUpdate.getTitleStatus());
		assertEquals("VINTEST", ofsllTitleUpdate.getVin());
		assertEquals("339351", ofsllTitleUpdate.getVlpref());		
	}
	
	@Test
	void testSearchRecordsSuccess_DoesNotThrowException() throws Exception {
		when(oAuth2RestTemplate.postForEntity(Mockito.anyString(), 
				  							  Mockito.any(Object.class), 
				  							  Mockito.eq(SearchSummaryResponse.class)))
		.thenReturn(successResponse);
				
		service.searchRecords(testSearchList);
		assertThatNoException();		
	}
	
	@Test
	void testCallSearchSummaryOAuthFailure_ThrowsSearchSummaryException() throws Exception {
		when(oAuth2RestTemplate.postForEntity(Mockito.anyString(), 
				  							  Mockito.any(Object.class), 
				  							  Mockito.eq(SearchSummaryResponse.class)))
		.thenThrow(new OAuth2Exception("Test OAuth2Exception"));
				
						
		assertThrows(SearchSummaryException.class, () -> service.searchRecords(testSearchList));		
	}
	
	@Test
	void testCallSearchSummaryService_ReturnsUnsuccessfulTransactionWhenUnsuccessfulResponse() throws Exception {
		when(oAuth2RestTemplate.postForEntity(Mockito.anyString(), 
				  							  Mockito.any(Object.class), 
				  							  Mockito.eq(SearchSummaryResponse.class)))
		.thenReturn(unsuccessfulResponse);
		
		OFSLLTitleUpdate titleUpdate = service.callSearchSummaryService(testSearchList.get(0));
		
		assertFalse(titleUpdate.getSuccessTxn());		
	}
	
	@Test
	void testCallSearchSummaryServiceRestException_ThrowsRestClientException() throws Exception {
		when(oAuth2RestTemplate.postForEntity(Mockito.anyString(), 
				  							  Mockito.any(Object.class), 
				  							  Mockito.eq(SearchSummaryResponse.class)))
		.thenThrow(new RestClientException("Test RestClientException"));
						
		SearchRequest request = testSearchList.get(0);
		assertThrows(RestClientException.class, () -> service.callSearchSummaryService(request));		
	}
	
	@Test	
	@Disabled("Not generating JsonProcessingException as expected. Needs more research on this case before enabling.")
	void testCallSearchSummaryServiceJSonException_ThrowsCreateRecordException() throws Exception {
		// Generate a JsonProcessingException when ObjectWriter tries to serialize OFSLLSearch
		SearchRequest mockSearch = mock(SearchRequest.class);
		when(mockSearch.toString()).thenReturn(mockSearch.getClass().getName());
		
		assertThrows(SearchSummaryException.class, () -> service.callSearchSummaryService(mockSearch));		
	}
	
	@Test
	void testSearchSummarySuccess_DoesNotThrowException() throws Exception {
		when(oAuth2RestTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.eq(SearchSummaryResponse.class))).thenReturn(successResponse);
				
		service.searchRecords(testSearchList);
		assertThatNoException();		
	}
	
	@Test
	void generateFilenameReturnsCorrectFileNameForCurrentDate() throws Exception {
		LocalDateTime ldt = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMddyyyy_HHmmss", Locale.ENGLISH);
		
		assertEquals(ofsllOutputDirectory + "Lien_search_" + ldt.format(dtf) + ".dat", service.generateCurrentFileName());
	}
	
	@Test
	void responseHasErrorReturnsTrueForValidFlagFalse() throws Exception {
		SearchSummaryResponse searchSummaryResponse = new SearchSummaryResponse();
		searchSummaryResponse.setValid(false);
		ResponseEntity<SearchSummaryResponse> response = new ResponseEntity<SearchSummaryResponse>(searchSummaryResponse, HttpStatus.OK);
		
		assertTrue(service.responseHasError(response));		
	}
	
	@Test
	void responseHasErrorReturnsTrueForNon200ResponseCode() throws Exception {
		SearchSummaryResponse searchSummaryResponse = new SearchSummaryResponse();
		searchSummaryResponse.setValid(true);
		ResponseEntity<SearchSummaryResponse> response = new ResponseEntity<SearchSummaryResponse>(searchSummaryResponse, HttpStatus.BAD_REQUEST);
		
		assertTrue(service.responseHasError(response));		
	}
	
	@Test
	void responseHasErrorReturnsTrueForNullResponse() throws Exception {
		ResponseEntity<SearchSummaryResponse> response = null;
		
		assertTrue(service.responseHasError(response));		
	}
	
	@Test
	void responseHasErrorReturnsFalseForValidFlagTrue() throws Exception {
		SearchSummaryResponse searchSummaryResponse = new SearchSummaryResponse();
		searchSummaryResponse.setValid(true);
		
		ResponseEntity<SearchSummaryResponse> response = new ResponseEntity<SearchSummaryResponse>(searchSummaryResponse, HttpStatus.OK);
		
		assertFalse(service.responseHasError(response));		
	}
	
	@Test
	void responseHasErrorReturnsTrueForConfirmationErrorStatus() throws Exception {
		SearchSummaryResponse searchSummaryResponse = new SearchSummaryResponse();
		searchSummaryResponse.setValid(true);
		Confirmation conf = new Confirmation();
		conf.setMessage("No records found for the specified search criteria");
		searchSummaryResponse.setConfirmation(conf);
		
		ResponseEntity<SearchSummaryResponse> response = new ResponseEntity<SearchSummaryResponse>(searchSummaryResponse, HttpStatus.OK);
		
		assertTrue(service.responseHasError(response));		
	}
	
	@Test
	void responseHasErrorReturnsTrueForNullResponseBody() throws Exception {
		SearchSummaryResponse searchSummaryResponse = null;
		ResponseEntity<SearchSummaryResponse> response = new ResponseEntity<SearchSummaryResponse>(searchSummaryResponse, HttpStatus.OK);
		
		assertTrue(service.responseHasError(response));		
	}
	@Test
	void writeResultToOutput_SuccessfullyGeneratesFile() throws Exception {
		assertDoesNotThrow(() -> service.writeResultToOutput(testOfsllTitleUpdateList));
	}
}
