package com.kubota.kcc.middleware.titlemgmt.searchsummary.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.kubota.kcc.middleware.titlemgmt.searchsummary.domain.OFSLLTitleUpdate;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.domain.SearchRequest;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.model.Confirmation;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.model.SearchSummaryResponse;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.model.Vehicle;

class OFSLLTitleUpdateMapperTest {
	OFSLLTitleUpdateMapper mapper = new OFSLLTitleUpdateMapper();
	
	SearchRequest validSearch;
	SearchRequest invalidSearch;
	SearchSummaryResponse successfulSearchSummaryResponse;
	SearchSummaryResponse unsuccessfulSearchSummaryResponse;
	SearchSummaryResponse unsuccessfulSearchSummaryResponseNoConf;
	
	@BeforeEach
	public void setup() {
		validSearch = buildValidTestSearch();
		invalidSearch = buildInvalidTestSearch();
		successfulSearchSummaryResponse = buildSuccessfulSearchResponse();
		unsuccessfulSearchSummaryResponse = buildUnsuccessfulSearchResponse();
		unsuccessfulSearchSummaryResponseNoConf = buildUnsuccessfulSearchResponseNoConf();
	}
	
	SearchRequest buildValidTestSearch() {
		SearchRequest search = new SearchRequest();

		search.setAccountNumber("12345678");
		search.setAssetNumber("500");
		search.setVin("KUB912830AZ");
		search.setVlref("338181");

		return search;
	}
	
	SearchRequest buildInvalidTestSearch() {
		SearchRequest search = new SearchRequest();

		search.setAccountNumber("55555555");
		search.setAssetNumber("200");
		search.setVin("KUB912850CA");
		search.setVlref("");

		return search;
	}
	
	SearchSummaryResponse buildSuccessfulSearchResponse() {
		SearchSummaryResponse response = new SearchSummaryResponse();
		
		response.setItemType("SearchSummary");
		response.setOrderReferenceId("785ff21e-eedb-4ce6-a6eb-d08011c04471");
		response.setValid(true);
		response.setVehicleCount(new BigInteger("1"));
		
		Vehicle vehicle = new Vehicle();
		vehicle.setBillCode("Ref-Test-1");
		vehicle.setBorrowerName("JOHN DOE");
		vehicle.setTitleState("TX");
		vehicle.setTitleStatus("Pending Title Notification");
		vehicle.setType("Title Storage");
		vehicle.setVin("KUB912830AZ");
		vehicle.setVlpref(new BigInteger("338181"));
		
		List<Vehicle> vehicleList = new ArrayList<>();
		vehicleList.add(vehicle);
		
		response.setVehicles(vehicleList);
		return response;
	}
	
	SearchSummaryResponse buildUnsuccessfulSearchResponse() {
		SearchSummaryResponse response = new SearchSummaryResponse();
		
		response.setItemType("SearchSummary");
		response.setOrderReferenceId("785ff21e-eedb-4ce6-a6eb-d08011c04471");
		response.setValid(true);
		
		Confirmation conf = new Confirmation();
		conf.setMessage("No records found for the specified search criteria");
		
		response.setConfirmation(conf);
		
		return response;
	}	
	
	SearchSummaryResponse buildUnsuccessfulSearchResponseNoConf() {
		SearchSummaryResponse response = new SearchSummaryResponse();
		
		response.setItemType("SearchSummary");
		response.setOrderReferenceId("785ff21e-eedb-4ce6-a6eb-d08011c04471");
		response.setValid(true);
		
		return response;
	}
	
	@Test
	void testMapTitleUpdate_Success() {
		OFSLLTitleUpdate update = mapper.mapSuccessfulTransaction(validSearch, successfulSearchSummaryResponse);
		
		assertEquals("500", update.getAssetNumber());
		assertEquals("12345678", update.getLoanAccountNbr());
		assertTrue(update.getSuccessTxn());
		assertEquals("Pending Title Notification", update.getTitleStatus());
		assertEquals("Title Storage", update.getTitleType());
		assertEquals("KUB912830AZ", update.getVin());
		assertEquals("338181", update.getVlpref());		
	}
	
	@Test
	void testMapUnsuccessfulTransaction_Success() {
		OFSLLTitleUpdate update = mapper.mapUnsuccessfulTransaction(invalidSearch, unsuccessfulSearchSummaryResponse);
		
		assertEquals("200", update.getAssetNumber());
		assertEquals("55555555", update.getLoanAccountNbr());
		assertFalse(update.getSuccessTxn());
		assertEquals("KUB912850CA", update.getVin());
		assertEquals("", update.getVlpref());		
		assertEquals("Error", update.getTitleStatus());
		assertEquals("No records found for the specified search criteria", update.getComment());
	}
	
	@Test
	void testMapUnsuccessfulTransaction_NoConfMessage() {
		OFSLLTitleUpdate update = mapper.mapUnsuccessfulTransaction(invalidSearch, unsuccessfulSearchSummaryResponseNoConf);
		
		assertEquals("200", update.getAssetNumber());
		assertEquals("55555555", update.getLoanAccountNbr());
		assertFalse(update.getSuccessTxn());
		assertEquals("KUB912850CA", update.getVin());
		assertEquals("", update.getVlpref());		
		assertEquals("Error", update.getTitleStatus());
		assertEquals("No API Confirmation Message", update.getComment());
	}
	
	@Test
	void testMapUnsuccessfulTransaction_NullResponse() {
		OFSLLTitleUpdate update = mapper.mapUnsuccessfulTransaction(invalidSearch, null);
		
		assertEquals("200", update.getAssetNumber());
		assertEquals("55555555", update.getLoanAccountNbr());
		assertFalse(update.getSuccessTxn());
		assertEquals("KUB912850CA", update.getVin());
		assertEquals("", update.getVlpref());		
		assertEquals("Error", update.getTitleStatus());
		assertEquals("Middleware Service Exception. Contact KCC App Support.", update.getComment());
	}
	
}
