package com.kubota.kcc.middleware.titlemgmt.searchsummary.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.kubota.kcc.middleware.titlemgmt.searchsummary.domain.SearchRequest;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.exception.SearchSummaryException;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.model.OrderHeader;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.model.SearchCriteria;
import com.kubota.kcc.middleware.titlemgmt.searchsummary.model.SearchSummaryRequest;


class WKSearchSummaryRequestMapperTest {
	WKSearchSummaryRequestMapper mapper = new WKSearchSummaryRequestMapper();
	
	SearchRequest validSearch;
	SearchRequest invalidSearch;
	
	@BeforeEach
	public void setup() {
		validSearch = buildValidTestSearch();
		invalidSearch = buildInvalidTestSearch();
	}
	
	SearchRequest buildValidTestSearch() {
		SearchRequest search = new SearchRequest();

		search.setAccountNumber("12345678");
		search.setAssetNumber("500");
		search.setVin("KUB912830AZ");
		search.setVlref("338181");

		return search;
	}
	
	SearchRequest buildInvalidTestSearch() {
		SearchRequest search = new SearchRequest();

		search.setAccountNumber("12345678");
		search.setAssetNumber("500");
		search.setVin("KUB912830AZ");
		search.setVlref("");

		return search;
	}
	
	@Test
	void testMapSearchSummaryRequest_Success() throws Exception {
		SearchSummaryRequest request = mapper.mapSearchSummaryRequest(validSearch);
		
		assertNotNull(request.getOrderHeader());
		assertNotNull(request.getOrderItems());
		assertNotNull(request.getSearchCriteria());
		assertEquals(1, request.getOrderItems().size());
	}
	
	@Test
	void testMapSearchSummaryRequest_InvalidSearchThrowsException() throws Exception {
		assertThrows(SearchSummaryException.class, () -> mapper.mapSearchSummaryRequest(invalidSearch));
	}
	
	@Test
	void testMapOrderHeader_Success() throws Exception {
		OrderHeader header = mapper.mapOrderHeader();
		
		assertEquals("ILIENMV.TITLEMANAGEMENT", header.getOrderType());
	}
	
	@Test
	void testMapSearchCriteria_Success() throws Exception {
		SearchCriteria criteria = mapper.mapSearchCriteria(validSearch);
		assertEquals("338181", criteria.getSearchKey());
	}
	
	@Test
	void testMapSearchCriteria_MissingVlrefThrowsException() throws Exception {
		assertThrows(SearchSummaryException.class, () -> mapper.mapSearchCriteria(invalidSearch));
	}
}


